import { FETCH_COUNTRIES_REQUEST, FETCH_COUNTRIES_SUCCESS, FETCH_COUNTRIES_ERROR, UPDATE_COUNTRY } from '../store/modules/country/types';
import { fetchCountriesRequest, fetchCountriesSuccess, fetchCountriesError, updateCountry } from '../store/modules/country/action';

describe('country actions', () => {

  it('should fire request action for countries FETCH', () => {
    const expectedAction = {
      type: FETCH_COUNTRIES_REQUEST,
    }
    expect(fetchCountriesRequest()).toEqual(expectedAction)
  })

  it('should fire success action for countries FETCH', () => {
    const countries = {
      country: {
        items: [
          {
            name: 'Afghanistan',
            capital: 'Kabul',
            area: 652230,
            population: 27657145,
            topLevelDomains: [
              {
                name: '.af'
              }
            ],
            flag: {
              svgFile: 'https://restcountries.eu/data/afg.svg'
            }
          },
          {
            name: 'Åland Islands',
            capital: 'Mariehamn',
            area: 1580,
            population: 28875,
            topLevelDomains: [
              {
                name: '.ax'
              }
            ],
            flag: {
              svgFile: 'https://restcountries.eu/data/ala.svg'
            }
          }
        ]
      }
    };
    const expectedAction = {
      type: FETCH_COUNTRIES_SUCCESS,
      payload: {
        countries
      }
    }
    expect(fetchCountriesSuccess(countries)).toEqual(expectedAction)
  })

  it('should fire error action for countries FETCH', () => {
    const expectedAction = {
      type: FETCH_COUNTRIES_ERROR,
    }
    expect(fetchCountriesError()).toEqual(expectedAction)
  })
  it('should fire success action for countries UPDATE', () => {
    const country = {
      name: 'Afghanistan',
      capital: 'Kabul',
      area: 652230,
      population: 27657145,
      topLevelDomains: [
        {
          name: '.af'
        }
      ],
      flag: {
        svgFile: 'https://restcountries.eu/data/afg.svg'
      }
    };
    const expectedAction = {
      type: UPDATE_COUNTRY,
      payload: {
        country
      }
    }
    expect(updateCountry(country)).toEqual(expectedAction)
  })
})

