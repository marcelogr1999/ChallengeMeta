import { FETCH_COUNTRIES_SUCCESS, FETCH_COUNTRIES_REQUEST, FETCH_COUNTRIES_ERROR, UPDATE_COUNTRY } from '../store/modules/country/types';
import countries, { selectCountriesByName, selectCountryById } from './../store/modules/country/reducer';

describe('country reducer', () => {
  const INITIAL_STATE = {
    items: [],
    status: 'idle',
    fetched: false,
  }
  const countriesList =
    [
      {
        name: 'Afghanistan',
        capital: 'Kabul',
        area: 652230,
        population: 27657145,
        topLevelDomains: [
          {
            name: '.af'
          }
        ],
        flag: {
          svgFile: 'https://restcountries.eu/data/afg.svg'
        }
      },
      {
        name: 'Åland Islands',
        capital: 'Mariehamn',
        area: 1580,
        population: 28875,
        topLevelDomains: [
          {
            name: '.ax'
          }
        ],
        flag: {
          svgFile: 'https://restcountries.eu/data/ala.svg'
        }
      }
    ];
  const countriesListWithId = countriesList.map((country, index) => {
    const domain = country.topLevelDomains.length > 0 ? country.topLevelDomains[0].name : '';
    return { ...country, id: index + 1, domain }
  });
  const initializedState = {
    country: {
      ...INITIAL_STATE,
      items: countriesListWithId
    }
  }

  it('should return the initial state', () => {
    expect(countries(undefined, {})).toEqual(INITIAL_STATE)
  })

  it('should handle FETCH_COUNTRIES_REQUEST', () => {
    expect(
      countries(INITIAL_STATE, {
        type: FETCH_COUNTRIES_REQUEST
      })
    ).toEqual(
      {
        items: [],
        status: 'loading',
        fetched: false,
      }
    )
  })

  it('should handle FETCH_COUNTRIES_SUCCESS', () => {
    expect(
      countries(INITIAL_STATE, {
        type: FETCH_COUNTRIES_SUCCESS,
        payload: {
          countries: countriesList
        }
      })
    ).toEqual(
      {
        items: countriesListWithId,
        status: 'idle',
        fetched: true,
      }
    )
  })

  it('should handle FETCH_COUNTRIES_ERROR', () => {
    expect(
      countries(INITIAL_STATE, {
        type: FETCH_COUNTRIES_ERROR
      })
    ).toEqual(
      {
        items: [],
        status: 'idle',
        fetched: false,
      }
    )
  })

  it('should handle UPDATE_COUNTRY', () => {
    const updatedCountry = {...initializedState.country.items[0], name: 'Afeganistão'}
    expect(
      countries(initializedState.country, {
        type: UPDATE_COUNTRY,
        payload: {
          country: updatedCountry
        }
      })
    ).toEqual(
      {
        items: [updatedCountry, initializedState.country.items[1]],
        status: 'idle',
        fetched: false,
      }
    )
  })

  it('should use selector selectCountriesByName', () => {
    expect(
      selectCountriesByName(initializedState, 'af')
    ).toEqual([countriesListWithId[0]])
  })

  it('should use selector selectCountryById', () => {
    expect(
      selectCountryById(initializedState, 2)
    ).toEqual(countriesListWithId[1])
  })
})