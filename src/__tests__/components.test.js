import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router';
import App from '../App';
import { Country } from '../pages/Country/index';
import { CountryItem } from '../pages/Country/CountryItem';
import { CountryDetails } from '../pages/CountryDetails/index';

import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
const middlewares = [thunk];
const mockStore = configureStore(middlewares);
import { Provider } from 'react-redux';

const INITIAL_STATE = {
  country: {
    items: [
      {
        name: 'Afghanistan',
        capital: 'Kabul',
        area: 652230,
        population: 27657145,
        topLevelDomains: [
          {
            name: '.af'
          }
        ],
        flag: {
          svgFile: 'https://restcountries.eu/data/afg.svg'
        },
        id: 1
      },
      {
        name: 'Åland Islands',
        capital: 'Mariehamn',
        area: 1580,
        population: 28875,
        topLevelDomains: [
          {
            name: '.ax'
          }
        ],
        flag: {
          svgFile: 'https://restcountries.eu/data/ala.svg'
        },
        id: 2
      }
    ],
    status: 'idle',
    fetched: false,
  }
}

const store = mockStore(INITIAL_STATE)

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'), // use actual for all non-hook parts
  useParams: () => ({
    countryId: 1,
  }),
}));

test('renders app', () => {
  render(<App />);
});

describe('renders Country', () => {
  it('should render Country', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <Country />
        </MemoryRouter>
      </Provider>
    );
  });
});

describe('renders CountryDetails', () => {
  it('should render CountryDetails', () => {
    render(<Provider store={store}><CountryDetails /></Provider>);
  });
});

describe('renders CountryItem', () => {
  it('should render CountryItem', () => {
    const country = INITIAL_STATE.country.items[0];
    render(<MemoryRouter><CountryItem country={country} /></MemoryRouter>);
  });
});
