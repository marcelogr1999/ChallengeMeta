import fetchData from './../../../services/routes/country';
import { FETCH_COUNTRIES_REQUEST, FETCH_COUNTRIES_SUCCESS, FETCH_COUNTRIES_ERROR, UPDATE_COUNTRY } from './types';

export function fetchCountriesRequest() {
  return {
    type: FETCH_COUNTRIES_REQUEST,
  };
}

export function fetchCountriesSuccess(countries) {
  return {
    type: FETCH_COUNTRIES_SUCCESS,
    payload: {
      countries
    }
  };
}

export function fetchCountriesError() {
  return {
    type: FETCH_COUNTRIES_ERROR,
  };
}

export function updateCountry(country) {
  return {
    type: UPDATE_COUNTRY,
    payload: {
      country
    }
  };
}

export function fetchCountriesAsync() {
  return (dispatch) => {
    dispatch(fetchCountriesRequest());
    fetchData().then(response => {
      console.log(response)
      dispatch(fetchCountriesSuccess(response.data.data.Country));
    }).catch(err => {
      console.log(err)
      dispatch(fetchCountriesError());
    });
  };
}