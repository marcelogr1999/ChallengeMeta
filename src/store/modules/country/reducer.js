import produce from 'immer';
import {
    FETCH_COUNTRIES_REQUEST,
    FETCH_COUNTRIES_SUCCESS,
    FETCH_COUNTRIES_ERROR,
    UPDATE_COUNTRY
} from './types';

const INITIAL_STATE = {
    items: [],
    status: 'idle',
    fetched: false,
};

const countries = (state = INITIAL_STATE, action) => {
    return produce(state, draft => {
        switch (action.type) {
            case FETCH_COUNTRIES_REQUEST: {
                draft.status = 'loading';
                break;
            }
            case FETCH_COUNTRIES_SUCCESS: {
                draft.items = action.payload.countries.map((country, index) => {
                    const domain = country.topLevelDomains.length > 0 ? country.topLevelDomains[0].name : '';
                    return { ...country, id: index + 1, domain }
                })
                draft.fetched = true;
                draft.status = 'idle';
                break;
            }
            case FETCH_COUNTRIES_ERROR: {
                draft.status = 'idle';
                break;
            }
            case UPDATE_COUNTRY: {
                const foundItemIndex = state.items.findIndex(item => item.id === action.payload.country.id);
                draft.items[foundItemIndex] = { ...state.items[foundItemIndex], ...action.payload.country };
                break;
            }
            default:
                return draft;
        }
    })
}

export const selectCountriesByName = (state, value) => {
    return state.country.items.filter(item => item.name.toUpperCase().includes(value.toUpperCase()));
}

export const selectCountryById = (state, value) => {
    return state.country.items.find(item => item.id === parseInt(value));
}

export default countries;