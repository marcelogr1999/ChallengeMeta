import { useDispatch, useSelector } from 'react-redux';

import styles from './CountryDetails.module.scss';
import { fetchCountriesAsync, updateCountry} from '../../store/modules/country/action'
import { useParams, useHistory } from 'react-router-dom';
import { useFormik } from 'formik';
import { useEffect, useState } from 'react';
import { selectCountryById } from '../../store/modules/country/reducer';

export function CountryDetails() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { countryId } = useParams();
  const country = useSelector(state => selectCountryById(state, countryId));
  const fetched = useSelector(state => state.country.fetched);
  const [edit, setEdit] = useState(false);

  useEffect(() => {
    if (!fetched)
      dispatch(fetchCountriesAsync());
  }, [dispatch, fetched]);

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: country?.name,
      capital: country?.capital,
      area: country?.area,
      population: country?.population,
      domain: country?.domain,
    },
    onSubmit: values => {
      if (!edit)
        setEdit(true);
      else {
        const formattedValues = {
          ...values,
          area: parseInt(values.area),
          population: parseInt(values.population),
          id: parseInt(countryId)
        };
        dispatch(updateCountry(formattedValues));
        setEdit(false);
      }
    },
  });

  return (
    <div className={styles.container}>
      <div className={styles.imgContainer}>
        <img src={country?.flag.svgFile} alt={country?.name} />
      </div>
      <form onSubmit={formik.handleSubmit}>
        <label htmlFor="name">Nome</label>
        <input
          id="name"
          name="name"
          type="text"
          disabled={!edit}
          onChange={formik.handleChange}
          defaultValue={formik.values.name}
        />
        <label htmlFor="capital">Capital</label>
        <input
          id="capital"
          name="capital"
          type="text"
          disabled={!edit}
          onChange={formik.handleChange}
          defaultValue={formik.values.capital}
        />
        <label htmlFor="area">Área</label>
        <input
          id="area"
          name="area"
          type="text"
          disabled={!edit}
          onChange={formik.handleChange}
          defaultValue={formik.values.area}
        />
        <label htmlFor="population">População</label>
        <input
          id="population"
          name="population"
          type="text"
          disabled={!edit}
          onChange={formik.handleChange}
          defaultValue={formik.values.population}
        />
        <label htmlFor="domain">Domínio</label>
        <input
          id="domain"
          name="domain"
          type="text"
          disabled={!edit}
          onChange={formik.handleChange}
          defaultValue={formik.values.domain}
        />
        <div className={styles.buttonsContainer}>
          <button type="button" onClick={() => history.push('/')}>Voltar</button>
          <button className={edit ? styles.save : ''} type="submit">{edit ? 'Salvar' : 'Editar'}</button>
        </div>
      </form>
    </div>
  );

}
