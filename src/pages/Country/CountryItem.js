import { Link } from 'react-router-dom';
import styles from './Country.module.scss';

export function CountryItem({country}) {
    return (<Link to={`/details/${country.id}`}>
        <div className={styles.card}>
            <img src={country.flag.svgFile} alt={country.name} />
            <div className={styles.info}>
                <span>{country.name}</span>
                <p>{country.capital}</p>
            </div>
        </div>
    </Link>
    )
}