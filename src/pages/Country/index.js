
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import styles from './Country.module.scss';

import { fetchCountriesAsync } from '../../store/modules/country/action'
import { selectCountriesByName } from './../../store/modules/country/reducer';
import { CountryItem } from './CountryItem';

export function Country() {
  const dispatch = useDispatch();
  const [searchName, setSearchName] = useState('');

  const countries = useSelector(state => selectCountriesByName(state, searchName));
  const status = useSelector(state => state.country.status);
  const fetched = useSelector(state => state.country.fetched);

  useEffect(() => {
    if (!fetched)
      dispatch(fetchCountriesAsync());
  }, [dispatch, fetched]);

  return (
    <div className={styles.container}>
      <input className={styles.searchBar} placeholder="Buscar país..." onChange={(e) => setSearchName(e.target.value)} />
      {status === 'loading' ? <h1 className={styles.loading}>Loading...</h1> : (
        <div className={styles.cardsContainer}>
          {countries &&
            countries.map(country => {
              return <CountryItem key={country.id} country={country} />
            })
          }
        </div>
      )}
    </div>
  );
}
