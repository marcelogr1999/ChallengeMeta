import { Provider } from 'react-redux';
import store from './store';
import { Country } from './pages/Country/index';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import { CountryDetails } from './pages/CountryDetails';
function App() {
  
  return (
    <Provider store={store}>
      <Router>
      <Switch>
        <Route path="/details/:countryId">
          <CountryDetails />
        </Route>
        <Route path="/">
          <Country />
        </Route>
      </Switch>
    </Router>
    </Provider>
  );
}

export default App;
