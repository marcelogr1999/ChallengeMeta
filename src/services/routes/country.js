// A mock function to mimic making an async request for data
import api from './../api';

export default function fetchData() {
  return api({
    method: 'post',
    data: {
      query: `
      query {
        Country {
          name
          capital
          area
          population
          topLevelDomains {
            name
          }
          flag {
            svgFile
          }
        }
      }
      `
    }
  })
}